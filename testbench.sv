// Code your testbench here
// or browse Examples
interface alu_intf();
  logic [5:0] opcode;
  logic [31:0] operand1;
  logic [31:0] operand2;
  logic [63:0] result;
endinterface : alu_intf

module top;
  class alu_tester;
    virtual alu_intf alu;
    
    randc bit[5:0] opcode;
    rand int unsigned operand1;
    rand int unsigned operand2;
    
    constraint opc { opcode >=0 && opcode <16;}
    
    task drive();
      alu.opcode = opcode;
      alu.operand1 = operand1;
      alu.operand2 = operand2;
    endtask : drive
    
  endclass : alu_tester
  
  // dut instance
  alu_intf alu(); 
  
  initial begin
    alu_tester tester;
    tester = new();
    tester.alu = alu;
    repeat(10) begin
      #10ns;
      tester.randomize();
      tester.drive();
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,top);
    #200ns;
    $finish();
  end
  logic [5:0] opcode;
  logic [31:0] operand1;
  logic [31:0] operand2;
  
  assign opcode = alu.opcode;
  assign operand1 = alu.operand1;
  assign operand2 = alu.operand2;
  
endmodule : top
  